package ljmu.vets;

public class InvoiceFileSaver {

    //The `InvoiceFileSaver` class handles saving invoice data (`Save` objects) to specified file paths in different file formats (DOCX, XPS, PDF).
    
    public void saveAs(Save save, FileType fileType, String path) {
        switch (fileType) {
            case DOCX: {
                saveAsDocX(save, path);
                break;
            }
            case XPS: {
                saveAsXPS(save, path);
                break;
            }
            case PDF: {
                saveAsPDF(save, path);
                break;
            }
        }
    }

    // Private method to save as DOCX
    private void saveAsDocX(Save save, String path) {
        // Implementation to save as DOCX
    }

    // Private method to save as XPS
    private void saveAsXPS(Save save, String path) {
        // Implementation to save as XPS
    }

    // Private method to save as PDF
    private void saveAsPDF(Save save, String path) {
        // Implementation to save as PDF
    }
}
